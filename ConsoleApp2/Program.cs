﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    class Node
    {
        public object data;
        public Node next;
    }
    class LinkedList
    {
        private Node head;

        public void AddBegining(object data)
        {
            Node el = new Node();
            el.data = data;
            el.next = head;
            head = el;
        }
        public void AddAfter(object data)
        {
            Node el = new Node();
            el.data = data;
            if (head == null)
            {
                head = el;
                el.next = null;
            }
            else
            {
                Node current = head;
                while (current.next != null)
                {
                    current = current.next;
                }

                current.next = el;
            }
        }
        public void PrintAllNodes()
        {
            Node current = head;
            while (current != null)
            {

                Console.WriteLine(current.data);
                current = current.next;

            }

        }
        public void Add(int index, object data)
        {
            Node el = new Node();
            el.data = data;
            int currentIndex = 0;
            Node current = head;
            while (currentIndex < index - 1)
            {
                current = current.next;
                if (current == null)
                {
                    break;
                }
                currentIndex++;
            }
            el.next = current.next;
            current.next = el;

        }
        public void Remove(int index)
        {
            int currentIndex = 0;
            Node current = head;
            while (currentIndex < index - 1)
            {
                current = current.next;
                currentIndex++;

            }
            //Console.WriteLine(current.data) ;
            //Console.WriteLine(currentIndex);
            Node el = current.next.next;
            current.next = el;

        }

    }

    class Program
    {
        static void Main(string[] args)
        {

            LinkedList myList = new LinkedList();

            myList.AddBegining("1");
            myList.AddBegining("2");
            myList.AddBegining(3);
            myList.Add(1, "4");

            myList.Remove(1);
            myList.AddAfter(5);
            myList.PrintAllNodes();
            Console.WriteLine();



            Console.ReadLine();
        }
    }
}
